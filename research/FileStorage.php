<?php

class FileStorage extends Storage
{


    public function create($object)
    {

        $way = $object->slug . '_' . date('Y-m-d') . '.txt';
        $i = 1;

        if (file_exists($way)) {
            $way = str_replace('.txt', '', $way) . '_' . $i . '.txt';
        }

        while (file_exists($way)) {
            $way = str_replace("$i.txt", "", $way) . ++$i . '.txt';
        }

        if (!file_exists($way)) {
            $object->slug = $way;
            $object->info['text'] = $object->text;
            $object->info['title'] = $object->title;
            $object->info['author'] = $object->author;
            $object->info['published'] = $object->published;
            file_put_contents($way, serialize($object));

        }
        return $way;
    }


    function read($slug)
    {
        if (file_exists($slug)) {
            return unserialize(file_get_contents($slug));
        }
    }

    function update($slug, $newObject)
    {
        if (file_exists($slug)) {
            file_put_contents($slug, serialize($newObject));
            if (file_put_contents($slug, serialize($newObject))) {
                echo 'Файл успішно редаговано';
            } else {
                echo 'От дідько.Щось пішло не так';
            }
        } else {
            echo 'Такого файлу не існує';
        }

    }

    function delete($slug)
    {
        if (file_exists($slug)) {
            unlink($slug);
            echo 'Файл успішно видалено';
        } else {
            echo 'такого файлу не існує';
        }
    }

    function list()
    {

        $mas = array_diff(scandir('C:\MAMP\htdocs\tasks\research\storage'), array('..', '.'));
        foreach ($mas as $item) {
            $masObjects[] = unserialize(file_get_contents('C:\MAMP\htdocs\tasks\research\storage/' . $item));
        }
        return $masObjects;
    }

}