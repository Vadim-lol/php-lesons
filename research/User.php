<?php


abstract class User
{
    public $id, $name, $role;

    abstract function getTextsToEdit();
}