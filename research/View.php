<?php


abstract class View
{
    public $storage;

    public function __construct($object)
    {
        $this->storage = $object;
    }

    abstract function displayTExtById($id);

    abstract function displayTextByUrl($url);

}