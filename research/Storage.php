<?php


abstract class Storage
{
    abstract function create($object);

    abstract function read($slug);

    abstract function update($slug, $newObject);

    abstract function delete($slug);

    abstract function list();
}