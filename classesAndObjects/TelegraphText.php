<?php


class TelegraphText
{
    public $title, $text, $author, $published, $slug;
    public $info = [
        'title' => [], 'text' => [], 'author' => [], 'published' => []
    ];

    public function __construct($title, $text, $author, $slug)
    {
        $this->author = $author;
        $this->title = $title;
        $this->text = $text;
        $this->slug = $slug;
        $this->published = date("F j, Y");
    }


    public function storeText()
    {
        $this->info['text'] = $this->text;
        $this->info['title'] = $this->title;
        $this->info['author'] = $this->author;
        $this->info['published'] = $this->published;
        file_put_contents($this->slug, serialize($this->info));
    }


    public function loadText()
    {
        if (file_exists($this->slug)) {
            if (filesize($this->slug) > 0) {
                $this->info = unserialize(file_get_contents($this->slug));
                $this->text = $this->info['text'];
                $this->title = $this->info['title'];
                $this->author = $this->info['author'];
                $this->published = $this->info['published'];

            } else {
                echo 'Файл пустий';
            }
        } else {
            echo 'Такого файлу не існує';
        }
        return $this->text;
    }


    public function editText($title, $text)
    {
        $this->loadText();
        $this->storeText($title, $text);
    }
}
