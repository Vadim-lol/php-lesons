<?php

$searchRoot = 'D:\xampp\htdocs\tasks\recursionCallbackAnonFunc\test_search';
$searchName = 'test.txt';
$searchResult = [];

//Функція для пошуку файла
function searchFile($searchRoot, $searchName, &$searchResult): array
{
    if (is_dir($searchRoot)) {
        $content = scandir($searchRoot);
        foreach ($content as $keys => $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            if (is_dir($searchRoot . '/' . $item)) {
                searchFile($searchRoot . '/' . $item, $searchName, $searchResult);
            } elseif (is_file($searchRoot . '/' . $item) || $item == $searchName) {
                if ($item == $searchName && filesize($searchRoot . '/' . $item) > 0) {
                    $searchResult[] = $searchRoot . '/' . $item;
                }
            }

        }
    }
    return $searchResult;
}

searchFile($searchRoot, $searchName, $searchResult);

if (empty($searchResult)) {
    echo 'Пошук не дав результатів';
} else {
    print_r($searchResult);
}
